package ru.tsc.korosteleva.tm;

import ru.tsc.korosteleva.tm.api.ICommandRepository;
import ru.tsc.korosteleva.tm.constant.ArgumentConst;
import ru.tsc.korosteleva.tm.constant.TerminalConst;
import ru.tsc.korosteleva.tm.model.Command;
import ru.tsc.korosteleva.tm.repository.CommandRepository;
import ru.tsc.korosteleva.tm.util.NumberUtil;

import java.util.Scanner;

public final class Application {

    private static final ICommandRepository COMMAND_REPOSITORY = new CommandRepository();

    public static void main(final String[] args) {
        if (processArgument(args)) close();
        showProcess();
    }

    public static void processCommand(final String command) {
        if (command == null || command.isEmpty()) return;
        switch (command) {
            case TerminalConst.ABOUT:
                showAbout();
                break;
            case TerminalConst.VERSION:
                showVersion();
                break;
            case TerminalConst.HELP:
                showHelp();
                break;
            case TerminalConst.INFO:
                showSystemInfo();
                break;
            case TerminalConst.COMMANDS:
                showCommands();
                break;
            case TerminalConst.ARGUMENTS:
                showArguments();
                break;
            case TerminalConst.EXIT:
                close();
                break;
            default:
                showError(command);
                break;
        }
    }

    public static boolean processArgument(final String[] args) {
        if (args == null || args.length == 0) return false;
        String arg = args[0];
        processArgument(arg);
        return true;
    }

    public static void processArgument(final String arg) {
        if (arg == null || arg.isEmpty()) return;
        switch (arg) {
            case ArgumentConst.ABOUT:
                showAbout();
                break;
            case ArgumentConst.VERSION:
                showVersion();
                break;
            case ArgumentConst.HELP:
                showHelp();
                break;
            case ArgumentConst.INFO:
                showSystemInfo();
                break;
            case ArgumentConst.COMMANDS:
                showCommands();
                break;
            case ArgumentConst.ARGUMENTS:
                showArguments();
                break;
            default:
                showError(arg);
                break;
        }
    }

    public static void showProcess() {
        showWelcome();
        final Scanner scanner = new Scanner(System.in);
        while (true) {
            System.out.println("ENTER COMMAND:");
            final String command = scanner.nextLine();
            processCommand(command);
        }
    }

    public static void showWelcome() {
        System.out.println("** WELCOME TO TASK-MANAGER **");
    }

    public static void showError(String arg) {
        System.err.printf("Error! This argument `%s` not supported. \n", arg);
    }

    public static void showAbout() {
        System.out.println("[ABOUT]");
        System.out.println("Name: Anastasia Korosteleva");
        System.out.println("E-mail: nnkorosteleva@gmail.com");
    }

    public static void showVersion() {
        System.out.println("[VERSION]");
        System.out.println("1.7.0");
    }

    public static void showHelp() {
        System.out.println("[HELP]");
        final Command[] commands = COMMAND_REPOSITORY.getTerminalCommands();
        for (final Command command : commands) System.out.println(command);
    }

    public static void showSystemInfo() {
        final Runtime runtime = Runtime.getRuntime();
        final int availableProcessors = runtime.availableProcessors();
        final long freeMemory = runtime.freeMemory();
        final String freeMemoryFormat = NumberUtil.formatBytes(freeMemory);
        final long maxMemory = runtime.maxMemory();
        final String maxMemoryValue = NumberUtil.formatBytes(maxMemory);
        final boolean maxMemoryLimit = maxMemory == Long.MAX_VALUE;
        final String maxMemoryFormat = maxMemoryLimit ? "no limit" : maxMemoryValue;
        final long totalMemory = runtime.totalMemory();
        final String totalMemoryFormat = NumberUtil.formatBytes(totalMemory);
        final long usedMemory = totalMemory - freeMemory;
        final String usedMemoryFormat = NumberUtil.formatBytes(usedMemory);
        System.out.println("Available processors (cores): " + availableProcessors);
        System.out.println("Free memory: " + freeMemoryFormat);
        System.out.println("Maximum memory: " + maxMemoryFormat);
        System.out.println("Total memory available to JVM: " + totalMemoryFormat);
        System.out.println("Used memory in JVM: " + usedMemoryFormat);
    }

    public static void showCommands() {
        final Command[] commands = COMMAND_REPOSITORY.getTerminalCommands();
        for (final Command command : commands) {
            final String name = command.getName();
            if (name == null || name.isEmpty()) continue;
            System.out.println(name);
        }
    }

    public static void showArguments() {
        final Command[] commands = COMMAND_REPOSITORY.getTerminalCommands();
        for (final Command command : commands) {
            final String argument = command.getArgument();
            if (argument == null || argument.isEmpty()) continue;
            System.out.println(argument);
        }
    }

    public static void close() {
        System.exit(0);
    }

}
